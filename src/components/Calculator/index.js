import React, { useReducer } from 'react'
import { ThemeProvider } from 'styled-components'

import theme from './theme'
import defaultSettings from './default-settings'
import reducer, { command } from './reducer'

import Keyboard from './Keyboard'
import Display from './Display'

const Calculator = ({ settings = defaultSettings.keybord }) => {

	const [displayText, dispatch] = useReducer(reducer, '0')

	return (
		<ThemeProvider theme={theme}>
			<>
				<Display text={displayText} size={4} />
				<Keyboard
					settings={settings}
					onPress={key => dispatch(command(key))}
				/>
			</>
		</ThemeProvider>
	)
}

export default Calculator
