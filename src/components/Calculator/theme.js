export default {
	buttonSize: 35,
	buttonMargin: 5,
	focusedColor: 'rgba(0, 0, 200, 0.3)',
	unfocusedColor: 'rgba(0, 0, 0, 0.3)'
}
