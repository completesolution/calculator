import styled from 'styled-components'

export const StyledRow = styled.div`
	height: ${({ theme }) => theme.buttonSize + theme.buttonMargin * 2}px;
	display: flex;
	justify-content: center;
	align-items: center;
`
