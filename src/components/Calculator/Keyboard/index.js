import React from 'react'

import Row from './Row'
import Button from './Button'

const Keybord = ({ settings, onPress }) =>
	settings.map((row, index) => (
		<Row key={index}>
			{row.map(key => (
				<Button
					label={key}
					key={key}
					onPress={() => onPress(key)}
				/>
			))}
		</Row>
	))

export default Keybord
