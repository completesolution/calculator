import styled from 'styled-components'

const boxShadow = insetColor =>
	({ pressed, theme }) => pressed ? `
		0 -1px 2px rgba(0, 0, 0, 0.3),
		0 1px 3px ${theme[insetColor]} inset
	` : `
		0 1px 3px rgba(0, 0, 0, 0.3),
		0 -1px 2px ${theme[insetColor]} inset
	`

export const StyledButton = styled.button`
	border: none;
	background: white;
	width: ${({ theme }) => theme.buttonSize}px;
	height: ${({ theme }) => theme.buttonSize}px;
	border-radius: 50%;

	box-shadow: ${boxShadow('unfocusedColor')};

	line-height: ${
		({ theme, pressed }) => theme.buttonSize - (pressed ? 0 : 3)
	}px;
	margin: ${({ theme }) => theme.buttonMargin}px;
	color: #777;
	outline: none;
	transition: box-shadow 0.2s, line-height 0.2s;

	&:hover {
		box-shadow: ${boxShadow('focusedColor')};
	}
`
