import React from 'react'
import { StyledButton } from './style'
import { useBooleanState, useDocumentEventHandler } from './hooks'

const Button = ({ label, onPress }) => {
	const [pressed, press, unpress] = useBooleanState()

	useDocumentEventHandler('mouseup', unpress)

	return (
		<StyledButton
			pressed={pressed}
			onMouseDown={press}
			onClick={onPress}
		>
			{label}
		</StyledButton>
	)
}

export default Button
