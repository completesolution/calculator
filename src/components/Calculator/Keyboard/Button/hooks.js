import { useState, useEffect } from 'react'

export const useBooleanState = (initialValue = false) => {
	const [value, setValue] = useState(initialValue)
	const setTrue = () => setValue(true)
	const setFalse = () => setValue(false)
	return [value, setTrue, setFalse]
}

export const useDocumentEventHandler = (eventName, handler) =>
	useEffect(() => {
		document.addEventListener(eventName, handler)
		return () => document.removeEventListener(eventName, handler)
	}, [])
