export const command = key => {
	switch (key) {
		case '=':
			return { type: 'equals' }
		case '<':
			return { type: 'erase' }
		default:
			if (/^\d$/.test(key)) {
				return {
					type: 'number',
					payload: key
				}
			}

			if (/^[\+\-\*\/]$/.test(key)) {
				return {
					type: 'operator',
					payload: key
				}
			}

			return {
				type: 'unknown',
				payload: key
			}
	}
}

export default (state, action) => {
	switch (action.type) {
		case 'number':
			return state === '0' ?
				action.payload :
				state + action.payload
		case 'operator':
			if (/[\+\-\*\/]$/.test(state)) {
				state = state.slice(0, -1)
			}

			return state + action.payload
		case 'erase':
			return state.slice(0, -1) || '0'
		case 'equals':
			try {
				return eval(state).toString()
			} catch(err) {
				return '0'
			}
		default:
			return state
	}
}
