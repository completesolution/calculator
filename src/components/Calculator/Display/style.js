import styled from 'styled-components'

export const StyledDisplay = styled.div`
	display: inline-block;
	margin: ${({ theme }) => theme.buttonMargin}px;
	border-radius: 5px;
	box-shadow:
		0 -1px 2px rgba(0, 0, 0, 0.3),
		0 1px 3px rgba(0, 0, 0, 0.3) inset;
	height: ${({ theme }) => theme.buttonSize}px;
	width: ${
		({ theme, size }) =>
			theme.buttonSize * size + theme.buttonMargin * 2 * size
	}px;
	line-height: ${({ theme }) => theme.buttonSize}px;
	text-align: right;
	padding-right: ${({ theme }) => theme.buttonMargin}px;
`
