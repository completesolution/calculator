import React from 'react'

import { StyledDisplay } from './style'

const Display = ({ text, size }) => (
	<StyledDisplay size={size}>
		{text}
	</StyledDisplay>
)

export default Display
